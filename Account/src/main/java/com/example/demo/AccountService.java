package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class AccountService {
	@Autowired
	AccountRepository accountrepository;
	public void save(AccountDetails accountdetails) {
		accountrepository.save(accountdetails);
		System.out.println(accountdetails);
	}
	public List<AccountDetails> getAccountDetailss() {
		return accountrepository.findAll();

	}
	
	public void getAccountDetailsByState(String state) {
		// TODO Auto-generated method stub
		accountrepository.findByState(state);
	}
	
}
