package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AccountDetails {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int accountid;
	private String ownername;
	private String address;

	private String state;
	private int pincode;


	private int balanceamount;


	public AccountDetails(String ownername, String address, String state, int pincode, int balanceamount,int accountid) {
		super();
		this.ownername = ownername;
		this.address = address;
		this.state = state;
		this.pincode = pincode;
		this.balanceamount = balanceamount;
	    this.accountid=accountid;
	}


	public String getOwnername() {
		return ownername;
	}


	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public int getPincode() {
		return pincode;
	}


	public void setPincode(int pincode) {
		this.pincode = pincode;
	}


	public int getBalanceamount() {
		return balanceamount;
	}


	public void setBalanceamount(int balanceamount) {
		this.balanceamount = balanceamount;
	}


	public int getAccountid() {
		return accountid;
	}


	public void setAccountid(int accountid) {
		this.accountid = accountid;
	}
	


}
