package com.example.spring;


public class Booking {
private String flightid;
private User user;
private Admin admins;
public Booking(String flightid, User user) {
	super();
	this.flightid = flightid;
	this.user = user;
	
}
public Booking(Admin admins) {
	super();
	this.admins = admins;
}
public String getFlightId() {
	return flightid;
}
public void setId(String flightid) {
	this.flightid = flightid;
}
public User getUser() {
	return user;
}
public void setUser(User user) {
	this.user = user;
}
public Admin getAdmins() {
	return admins;
}
public void setAdmins(Admin admins) {
	this.admins = admins;
}




}
