package com.example.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class AdminService {
	@Autowired
	AdminRepository adminRepository;
	public void save(Admin admin) {
		adminRepository.save(admin);
		System.out.println(admin);

}
	public List<Admin> getAdmins() {
		
		return  adminRepository.findAll();

	}
}
