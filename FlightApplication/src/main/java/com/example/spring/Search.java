package com.example.spring;

public class Search {
private int flightid;
private int name;
private String route;
private String timings;
public Search(int flightid, int name, String route, String timings) {
	super();
	this.flightid = flightid;
	this.name = name;
	this.route = route;
	this.timings = timings;
}
public int getFlightid() {
	return flightid;
}
public void setFlightid(int flightid) {
	this.flightid = flightid;
}
public int getName() {
	return name;
}
public void setName(int name) {
	this.name = name;
}
public String getRoute() {
	return route;
}
public void setRoute(String route) {
	this.route = route;
}
public String getTimings() {
	return timings;
}
public void setTimings(String timings) {
	this.timings = timings;
}

}
