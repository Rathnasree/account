package com.example.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;



@RestController
public class UserController {
	@Autowired
	UserService userService;
   AdminService adminService;
	 @GetMapping("/")
	 List<User>getUsers()  {
	        System.out.println("User Details");
	        return userService.getUsers();
	 }
	 @GetMapping("/{id}")
		void getAccountDetails(@PathVariable Integer id) {
			System.out.println("called .." +id);
		}
	    
@PostMapping("/User")
public String savedetails(@RequestBody User user)
{
	System.out.println("id:"+user.getId());
	System.out.println("name:"+user.getName());
	System.out.println("emailid:"+user.getEmailid());
	System.out.println("gender:"+user.getGender());
	System.out.println("age:"+user.getAge());
	System.out.println("meal:"+user.getMeal());
	System.out.println("seatnumber:"+user.getSeatnumber());
	//UserService.save(user);
	return "post called";
}
@PostMapping("/Admin")
public String savedetails(@RequestBody Admin admin) 
{
	
	adminService.save(admin);
	System.out.println("id:"+admin.getId());
	System.out.println("flightnumber:"+admin.getFlightnumber());
	System.out.println("airline:"+admin.getAirline());
	System.out.println("fromline:"+admin.getFromPlace());
	return "post called";
}
@PostMapping("/Search")
public String savedetails(@RequestBody Search search) {
	System.out.println("flightid:"+search.getFlightid());
	System.out.println("route:"+search.getRoute());
	System.out.println("timings:"+search.getTimings());
	return "post called";
}
@PostMapping("/Booking")
public String savedetails(@RequestBody Booking booking) {
	System.out.println("flightid:"+booking.getFlightId());
	return "post called";
}
}
