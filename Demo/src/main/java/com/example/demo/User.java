package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sun.istack.NotNull;

@Entity
public class User {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
//@NotNull
	private String name;
	private int age;
	public User(int id, String name,int age) {
		super();
		this.id = id;
		this.name = name;
		this.age=age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(int age) {
		this.age=age;
	}
	
}
