package com.example.demo;




import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;@RequestMapping("/user")
@RestController
public class UserController {//POJO
@Autowired
UserService userService;//dependency
@GetMapping("/")
List<User> getUsers()
{
System.out.println("called");
return userService.getUsers();
}
@GetMapping("/{id}")
void getUser(@PathVariable Integer id) {
System.out.println("Called "+id);
}
@PostMapping("/")
private Integer saveUser(@RequestBody User user) {//to create user
userService.save(user);
return userService.save(user);
}
@PutMapping("/")
String handlePUtMapping() {
System.out.println("put");
return "put method called";
}
@GetMapping("age/{age}")
void getUserByAge(@PathVariable("age")int age){
	userService.getUserByAge(age);
}
}

